function split(s)
  local table = {};
  local i = 1;
  local last = 1;
--  print(s:find("[^,%s]+", last))
  while true do
    local a, b = s:find("[^,%s]+", last);
    if(a == nil) then
      break
    end
    last = b+1;
    table[i] = s:sub(a, b);
    i=i+1;
  end
  return table;
end

colindicies = {};
colindicies.len = 0;
function writeCSV(buff)
  local upnil = 0;
  local bufflen = 0;
  local line = {};
  for b, _ in pairs(buff) do 
    bufflen = bufflen+1
    if(colindicies[b] == nil) then
      colindicies[b] = colindicies.len;
      colindicies.len = colindicies.len+1;
--      for a, b in pairs(colindicies) do print(a, b) end
    end
  end
  local runningindex = 1;
--  print("bufflen: "..bufflen)
  while upnil < bufflen do
--    for a, b in pairs(buff) do print("Buff pair:", a, b) end
    for a, b in pairs(buff) do
      if(b[runningindex] == nil) then
        upnil = upnil+1;
        break
      end
      line[colindicies[a]] = b[runningindex];
    end
--    for a, b in pairs(line) do print("Line column/val pair:", a, b) end
    local outline = ""
    for i=0,colindicies.len-1 do
--      print(i, line[i]);
      if(line[i] == nil) then
        line[i] = "";
      end
      outline = outline..line[i];
      if(i~=colindicies.len-1) then
        outline = outline..", ";
      end
    end
--    print("Writting line to file: ", outline);
    outfile:write(outline.."\n");
    runningindex = runningindex+1;
  end
end

outfile = assert(io.open(arg[2] or "/home/groomble/Downloads/sorteddata.csv","w+"));
outfile:setvbuf("full");
floatingbuffer = {};
for line in io.lines(arg[1] or "/home/groomble/Downloads/tempdata.csv") do
  local cells = split(line);
  if(not floatingbuffer[cells[1]]) then
    floatingbuffer[cells[1]] = {select(2, unpack(cells))};
--    for a, b in pairs(floatingbuffer) do print(a, b) end
  else -- hit a conflict
    outfile:write(writeCSV(floatingbuffer));
    floatingbuffer = {};
    floatingbuffer[cells[1]] = {select(2, unpack(cells))};
  end
end
outfile:flush();
outfile:close();
print("Done!");