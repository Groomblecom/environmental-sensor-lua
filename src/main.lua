mqttClient = nil
a = true; --autorun kill
f = false -- abbreviation to kill, a=f
configTable = nil;
triedDefaultSSID = false;
mqttClientDisconnected = true;
adcvalbuffer = {};
msoffbuffer = {};
tempbuffer = {};
humiditybuffer = {};

--load server, name, etc from config.json
function sensorInit()
  local lastLine = nil;
  local t = { };
  file.open("config.json", "r");
  repeat
    t[#t+1] = lastLine;
    lastLine = file.readline();
  until lastLine == nil;
  file.close();
  configTable = sjson.decode(table.concat(t,""))
end

function flushSensorBuffers()
  print("Flushing data buffer");
  if(wifi.sta.status() ~= 5) then 
    connectToWifi(flushSensorBuffers);
    return;
  end
  if(mqttClientDisconnected) then
    print("Reconnecting to MQTT server");
    mqttClient:connect(configTable["servers"], 1883, 0, 0, function() 
      -- MQTT client settings
      mqttClient:on("offline", function() mqttClientDisconnected = true; end)
      local statusTopic = string.format("/sensors/sensor%s/status", configTable["sensorindex"]);
      mqttClient:lwt(statusTopic, string.format("LWT sent; %s has unexpectedly lost connection", configTable["sensorhumanname"]), 2, 1)
      mqttClient:publish(statusTopic, string.format("ONLINE sent; %s has gained connection, bootreason:%s,%s", configTable["sensorhumanname"], node.bootreason()), 2, 1);
      print("MQTT connection reacquired")
      mqttClientDisconnected = false;
      flushSensorBuffers(serverindex);
    end, function(_, m) print("MQTT reconnect failed, retrying "..m); flushSensorBuffers(serverindex+1) end);
    return;
  else
    print("MQTT server already connected");
  end
  print("Publishing MQTT message");
  if(#adcvalbuffer == 0) then
    print("Skipping publish because of empty buffers");
    return;
  end
  repeat
    local function pop(t)
      return table.remove(t, 1)
    end
    local seconds;
    local microseconds;
    seconds, microseconds = rtctime.get();
    local timestamp = pop(msoffbuffer);
    seconds = seconds-timestamp[1];
    microseconds = microseconds-timestamp[2];
    local msoffVal = seconds*1000000+microseconds;
    local message = string.format("light:%s,temp:%s,humidity:%s,msoff:%s", pop(adcvalbuffer), pop(tempbuffer), pop(humiditybuffer), msoffVal/1000);
    mqttClient:publish(string.format("/sensors/sensor%s/data", configTable["sensorindex"]), message, 1, 1, function(conn)
      print("sent message");
    end);
  until #adcvalbuffer == 0;
  
end
function logData(count, adcval, tempval, humval)
  if(count == nil) then
    adcval = 0;
    tempval = 0;
    humval = 0;
    count = 0;
  end
  
  -- actually make measurements
  adcval = adcval+adc.read(0);
  local temp, humi = hdc1080.read()
  print("temp", temp)
  print("humi", humi)
  tempval = tempval + temp;
  humval = humval + humi;
  count = count+1;
  
  if(count >= 5) then
    table.insert(adcvalbuffer, adcval/count);
    table.insert(tempbuffer, tempval/count);
    table.insert(humiditybuffer, humval/count);
    local seconds, microseconds = rtctime.get();
    table.insert(msoffbuffer, {seconds, microseconds})
  else
    tmr.alarm(1, 100, tmr.ALARM_SINGLE, function() logData(count, adcval, tempval, humval) end)
  end
end

function connectToWifi(onSuccess)
  print("wifi connection attempt. wifi status: "..wifi.sta.status())
  if(wifi.sta.status() == 255) then
    wifi.setmode(wifi.STATION)
  end
  if(wifi.sta.status() == 0 or wifi.sta.status() == 3 or wifi.sta.status() == 4 or wifi.sta.status() == 2) then
    print("Connection being handled by first if")
    wifi.setmode(wifi.STATION);
    if(not triedDefaultSSID) then
      print("trying default SSID")
      configTable["defaultWifiConfig"].save = true
      configTable["defaultWifiConfig"].auto = true
      wifi.sta.config(configTable["defaultWifiConfig"]);
      triedDefaultSSID = true;
      tmr.alarm(1, 1000, tmr.ALARM_SINGLE, function() connectToWifi(onSuccess) end)
      return;
    else
      print("launching enduser_setup")
      enduser_setup.manual(false)
      enduser_setup.start(function() connectToWifi(onSuccess) end);
      return;
    end
  end
  if(wifi.sta.status() == 5) then
    print("wifi successfully obtained")
    onSuccess();
    return;
  end
  print("trying to connect again in 500ms")
  tmr.alarm(1, 500, tmr.ALARM_SINGLE, function() connectToWifi(onSuccess) end)
end

local function main()
  if(configTable == nil) then
    print("loading config")
    sensorInit();
  end
  -- The 'RTC' clock is used relatively, so it doesn't really matter what it's set to.
  rtctime.set(1480000000, 0)
  if(wifi.sta.status() ~= 5) then
    connectToWifi(function() main() end);
    return;
  end
  
  local name = string.format("sensor%s", configTable["sensorindex"]);
  mqttClient = mqtt.Client(name, 120*5+6, name, "None");
  
  -- SDA=5, SCL=6
  i2c.setup(0, 5, 6, i2c.SLOW)
  hdc1080.setup();
  
  logData();
  flushSensorBuffers();
  print("Starting logging loop");
  tmr.alarm(0, 30000, 1, function() 
    if(a== false) then tmr.stop(0) return; end
    logData();
    if #adcvalbuffer >= 5 then
      flushSensorBuffers();
    end
  end);
  
end
main()
