function init() 
  local exitimmediately = false
  
  function runMain()
    print("Executing main file");
    if(file.exists("main.lc")) then
      dofile("main.lc")
    else
      dofile("main.lua")
    end
  end
  
  function start()
    uart.on("data")
    if exitimmediately then
      print("Prompt open:\n>");
      return --do nothing, exiting to the interactive prompt
    else
      local extension = file.exists("main.lc") and "lc" or "lua"
      if(file.exists("main."..extension)) then
        local ok, err = pcall(runMain)
        if(not ok) then
          print("Falling back because of error in main: "..tostring(err));
          -- consider sentry integration here
          local oldExtension = file.exists("main.lc.old") and "lc" or "lua";
          if(file.exists("main."..oldExtension..".old")) then
            file.remove("main."..extension);
            file.rename("main."..oldExtension..".old", "main."..oldExtension)
          end
          node.restart();
        end
      end
    end
  end
  
  print("Press enter for interactive console");
  uart.on("data", "\r", function() exitimmediately=true; print("interrupted, exiting"); end, 0)
  tmr.alarm(2, 10000, 0, start);
end

init();