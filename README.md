Lua firmware for groomblecom's environmental sensors project

---

The format for the defaultWifiConfig config parameter is as per the nodemcu documentation for wifi.sta.config. Auto and save options are overridden at runtime, however.
